const mongoose = require('mongoose');

const DB_URL = `mongodb://localhost:27017/luckyproject`;

const connect = () => {
    mongoose.connect(DB_URL, {
        useNewUrlParser:true,
        useUnifiedTopology:true,
        useFindAndModify: false,
    })
    .then(()=> {
        console.log("Success connected to DB");
    })
    .catch(error => {
        console.log("Error connecting to DB: ", error);
    })
}

module.exports =  { DB_URL, connect};