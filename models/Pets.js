const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const petSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    zone: {
        type: String,
      
    },
    specie: {
        type: String,
        
    }, 
    breed: {
        type: String
    },
    dateBorn: {
        type: String
    },
    Size: {
        type: String
    },
    weight: {
        type: Number
    },
    description: {
        type: String
    },
    photo: {
        type: String
    },
    status: {   
        type: String,
      
    },
    vaccinated: {
        type: Boolean
    },
    dewormed: {
        type: Boolean
    },
    chip: {
        type: Boolean
    },
    
    adoptionForm:[{
        user:{
            type:String
        },     
        state:{
            type:String
        },
        userName: {
            type: String
        },
        userEmail: {
            type: String
        },
        userPhone: {
            type: Number
        },
        userDNI: {
            type: String
        },
        userDirection: {
            type: String
        },
        userCP: {
            type: Number
        },
        userCity: {
            type: String
        },
        userAnimals: {
            type: String
        },
        userAdopting: {
            type: String
        },
        petNecesities: {
            type: String
        },
        petCost: {
            type: String
        },
        petHealth: {
            type: String
        },

    }]
},
    {
        timestamps: true,
    },

);

const Pet = mongoose.model('Pet', petSchema);
module.exports = Pet;