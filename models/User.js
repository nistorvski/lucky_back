const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name:{
        type:String,
        
    },
    surname:{
        type:String,
        
    },
    email:{
        type:String,
        required:true,
    },
    password:{
        type:String,
        required:true,
    },
    cp:{
        type:Number,
    },
    phone:{
        type:String,
    },
    avatar:{
        type:String,
    },   
    adoption:{
        type:Boolean
    }
     // favorites:[{
    //     type:mongoose.Types.ObjectId,
    //     ref:'Favorite'
    // }],
   
   
},
  {
    timestamps: true,
  }
   
  );

const User = mongoose.model('User', userSchema);
module.exports= User;