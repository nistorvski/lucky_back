const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const adoptionSchema = new Schema (
    {
        name:{
            type:String,
            required:true,
        },
        surname:{
            type:String,
            requires:true,
        },
        email:{
            type:String,
            required:true,
        },
        phone:{
            type:String,
            required:true,
        },
        dni:{
            type:String,

        },
        city:{
            type:String,            
        },
        street:{
            type:String
        },
        number_street:{
            type:Number
        },
        user:[{
            type:mongoose.Schema.Types.ObjectId,
            ref:'User'
        }]

    },
    {
        timestamps:true
    }
);

const Adoption = mongoose.model('Adoption', adoptionSchema);
module.exports=Adoption;
