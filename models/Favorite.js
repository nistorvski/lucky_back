const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const favoriteSchema = new Schema({

    check:{
        type:Boolean,
        required:true,
    },
    user:{
        type:mongoose.Types.ObjectId,
        ref:"User",
    }

},
{
    timestamps:true
});

const Favorite = mongoose.model('Favorite', favoriteSchema);
module.exports=Favorite;