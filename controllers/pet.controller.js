const Pet = require('../models/Pets');



//Endpoint Get Pet------------
const getPets = async(req, res, next) => {
    try{
        const pets = await Pet.find().populate("user");
    return res.status(200).json(pets)
    }catch(error){
        return next(error);
    }
    
};


const sendAdoption = async (req, res, next) => {
    try{
     
        const {
            id,
            state,
            userName,
            userEmail,
            userPhone,
            userDNI,
            userDirection,
            userCP,
            userCity,
        }=req.body;

        const newAdoptedForm = await Pet.findByIdAndUpdate(id,{$push:{
           "adoptionForm":
               { 
                userName,
                state,             
                userEmail,               
                userPhone,
                userDNI,
                userDirection,
                userCP,
                userCity,
           }
        }},
        {
            new:true
        });

        return res.status(200).json(newAdoptedForm);

    }catch(error){
        return next(error);
    }
}


//Endpoint Create -------------
const createPet = async(req, res, next) => {
    try{
        const {
            name,
            zone,
            specie, 
            breed,
            dateBorn,
            Size,
            weight,
            description,
            photo,
            vaccinated,
            dewormed,
            chip
        } = req.body;

        const newPet = new Pet({
            name,
            zone,
            specie, 
            breed,
            dateBorn,
            Size,
            weight,
            description,
            photo,
            vaccinated,
            dewormed,
            chip
        });

        const createdPet = await newPet.save();
        return res.status(200).json(createdPet)
    }catch(err){
        next(err);
    }
}


//Endpoint Detalle de cada Animal---------
const getByIdPet = async(req, res, next) => {
    const id = req.params.id;

    try{
        let petFinded = await Pet.findById(id);
        if(!petFinded) {
            petFinded = false;
        }
        return res.status(200).json(petFinded)
    } catch (err) {
        next(err);
    }
}

//Endpoint Editar animal---------
const editPet = async(req, res, next) => {
    try{
        const { name, id } = req.body;
        const updatePet = await Pet.findByIdAndUpdate(
            id,
            { name: name },
            { new: true }
        );
        return res.status(200).json(updatePet);
    } catch(err) {
        next(err);
    }
}

//Endpoint eliminar animal-----
const deletePet = async(req, res, next) => {
    const { id } = req.params;
    try{
        await Pet.findByIdAndDelete(id);
        return res.status(200).json('Pet deleted!');
    } catch(err){
        next(err);
    }
}

module.exports = {
    createPet,
    getPets,
    getByIdPet,
    editPet,
    deletePet,
    sendAdoption,
}
