const Favorite = require('../models/Favorite');
const controller = {};



controller.getFavorite = async (req, res, next) => {
    try{

        const findFavorite = await Favorite.find().populate("user");

        return res.status(200).json(findFavorite);


    }catch(error){
        return next(error);
    }
}

controller.addFavortie = async (req, res, next)=> {
    try{
        const {check}=req.body;

        const newFavorite = new Favorite({
            check
        });
        const saveFavorite = await newFavorite.save();
        console.log("Este animal es favoooritoo", saveFavorite);

        return res.status(200).json(saveFavorite);

    }catch(error){
        return next(error);
    }
};

controller.deleteFavorite = async (req, res, next) => {
    try{
        const { id}=req.body;

            await Favorite.findByIdAndDelete(id);
            return res.status(200);
          

    }catch(error){
        return next(error);
    }

}

module.exports = controller;
