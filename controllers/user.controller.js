const passport = require('passport');
const User = require('../models/User');

module.exports = { 

    checkSession: async (req, res, next) => {
        if(req.user) {
          return res.status(200).json(req.user);
        } else {
          return res.status(401).json({message: 'No user found'});
        }
    },

    getUser: async (req, res, next) => {
    try{
        const users = await User.find();
     
        return res.status(200).json(users);
    }catch(error){
        return next(error);
    }
},


    userRegister: (req, res, next) => {
    passport.authenticate('register', (error, user)=> {
        if(error) {
            return response.status(400).json({ error: error.message});
        }

        req.logIn(user, (error) => {
            if(error){
                return res.json({ error: error.message });
            }
            return res.json(user);
        });
    })(req, res, next);
},

    userLogin: (req, res, next)=> {
    passport.authenticate('login', (error, user) => {
        if(error){
          return res.json({ error: error.message});
        }

        req.logIn(user, (error) => {
            if(error){
              return res.json({ error: error.message});
            }
            return res.status(200).json(user);
        });

        
    })(req, res, next);
},

    userLogout: (req, res, next)=> {
    if(req.user) {
        req.logout();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');   
            return res.status(200).json({message: 'Logout successfull'})
        });
    }else{
        return res.sendStatus(403).json({message: 'Unexpected error'});
    }
},

    userEdit: async (req, res, next) => {

    try{
        const userAvatar = req.file ? req.file.filename : null ;

        const {
            id,
            email,
            name,
            surname,
            cp,
            phone,
        } =req.body;

        const findUserAndUpdate = await User.findByIdAndUpdate(
            id,
            {
            email:email.toLowerCase(),
            name,
            surname,
            cp,
            phone,
            avatar: userAvatar
            },
            {new:true}
        );

        //console.log("Encontrar el usuario", findUserAndUpdate);
         return res.status(200).json(findUserAndUpdate); 
       

    }catch(error){
        return next(error);
    }
},

    userDelete: async (req, res, next) => {
   
    try{
        const {id} = req.body;

        const findUserAndDelete = await User.findByIdAndRemove(id);

        if(!findUserAndDelete){
            const error = new Error("User dont exist");
            return next(error);
        }
              return res.status(200).json("EL ususario se ha elimibnado correctamente");       
              
        }catch(error){
        return next(error);
    }


}
}
