const Adoption = require('../models/Adoption');
const User = require('../models/User');
const controller = {};


controller.getAdoptions = async (req, res, next) => {
    try{

        const adoptions = await Adoption.find().populate('user');

        return res.status(200).json(adoptions);

    }catch(error){
        return next(error);
    }
};

controller.requestAdoption = async (req, res, next) => {
    try{
        const {
            name,
            surname,
            email,
            phone,
            dni,
            city,
            street,
            number_street,
        } = req.body;

        const newrequestAdoption = new Adoption( { 
            name,
            surname,
            email,
            phone,
            dni,
            city,
            street,
            number_street,
            
        });
        
        const createdRequestAdoption = await newrequestAdoption.save();

        console.log('EL formulario a sido enviado', createdRequestAdoption);
        return res.status(200).json(createdRequestAdoption);


    }catch(error){
        return next(error);
    }
};



module.exports=controller;


