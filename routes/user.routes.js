const express = require('express');
const userController = require('../controllers/user.controller');
const fileMiddlewares = require('../middlewares/file.middleware');
const router = express.Router();


router.get('/get_users', userController.getUser);
router.post('/register', [fileMiddlewares.upload.single('avatar')], userController.userRegister);
router.post('/login', userController.userLogin);
router.post('/logout', userController.userLogout);
router.put('/edit', userController.userEdit);
router.delete('/delete', userController.userDelete);

module.exports= router;
