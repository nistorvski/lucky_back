const express = require('express');
const adoptionController = require('../controllers/adoption.controller');
const router = express.Router();


router.get('/get_adoption', adoptionController.getAdoptions);
router.post('/create_adoption',adoptionController.requestAdoption);

module.exports = router;
