const express = require('express');

const fileMiddleware = require('../middlewares/file.middleware');

const petController = require('../controllers/pet.controller');

const router = express.Router();

router.get('/pets', petController.getPets);
router.post('/create', /*[fileMiddleware.upload.single("image"), fileMiddleware.uploadToCloudinary],*/ petController.createPet);
router.put('/edit', petController.editPet);
router.get('/:id', petController.getByIdPet);
router.get('/:id/delete', petController.deletePet);
router.post('/add_form', petController.sendAdoption);

module.exports = router;