const express = require('express');
const router = express.Router();
const favoriteController = require('../controllers/favorite.controller');


router.get('/get_favorite',favoriteController.getFavorite);
router.post('/add_favorite', favoriteController.addFavortie);
router.delete('/delete_favorite', favoriteController.deleteFavorite);


module.exports = router;
