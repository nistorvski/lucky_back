const express = require('express');
const passport = require('passport'); 
const mongoose = require('mongoose');
const cors = require('cors');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);


const PORT = 4005;
const app = express();
const db = require('./db');
db.connect();
require('./passport');

const userRoutes = require('./routes/user.routes');
const adoptionRoutes = require('./routes/adoption.routes');
const favoriteRoutes = require('./routes/favorite.routes');
const petsRoutes = require('./routes/pet.routes');

//passport

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
  });
  
  app.use(cors({
    origin: 'http://localhost:4000',
  }));
  

app.use(
    session({
      secret: process.env.SESSION_SECRET || 'upgradehub_node', // ¡Este secreto tendremos que cambiarlo en producción!
      resave: false, // Solo guardará la sesión si hay cambios en ella.
      saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
      cookie: {
        maxAge: 3600000 // Milisegundos de duración de nuestra cookie, en este caso será una hora.
      },
      store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
  );


app.use(passport.initialize());
app.use(passport.session());


//Para la request del body 
app.use(express.json());
app.use(express.urlencoded({extended:false}));


//Rutas
app.use('/user', userRoutes);
app.use('/adoption', adoptionRoutes);
app.use('/favorite',favoriteRoutes);
app.use('/pets', petsRoutes);

app.listen(PORT, () => {
    console.log(`This server is listening in localhost://${PORT}`);
});