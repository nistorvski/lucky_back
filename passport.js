const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('./models/User');

const bcrypt = require('bcrypt');
const saltRound=10;




// Esta función usará el usuario de req.LogIn para registrar su id en la cookie de sesión
passport.serializeUser((user, done) => {
    return done(null, user._id);
  });
  
  // Esta función buscará un usuario dada su _id en la DB y populará req.user si existe
  passport.deserializeUser(async (user, done) => {
    try {
      const existingUser = User.findById(user);
      return done(null, existingUser);
    } catch (err) {
      return done(err);
    }
  });


passport.use('register', new LocalStrategy({
    usernameField:'email',
    passwordField:'password',
    passReqToCallback:true,
},
async (req, email, password, done) => {
    try{

        const previousUser = await User.findOne({email: email.toLowerCase()});

        if(previousUser){
            const error = new Error('User is already exist');
            return done(error); 
        }

        const hash = await bcrypt.hash(password, saltRound);

        const userAvatar = req.file ? req.file.filename : null ;

        const {
          name,
          surname,
          cp,
          phone,
          
        } = req.body;

        const newUser = await new User(
            {
                email: email.toLowerCase(),
                password: hash,
                name,
                surname,
                cp,
                phone,
                avatar:userAvatar
            }
           
        );

        const savedUser = await newUser.save();

        return done(null, savedUser);

    }catch(error){
        return done(error);
    }
}
));


passport.use('login', new LocalStrategy(
{
    usernameField:'email',
    passwordField:'password',
    passReqToCallback:true,

}, async (req, email, password, done)=> {
    try{
        
        const currentUser = await User.findOne({email: email.toLowerCase()});

        if(!currentUser){
            const error = new Error('this user does not exist');
            return done(error);
        }

        const isValidPassword = await bcrypt.compare(
            password,
            currentUser.password
          )

          if (!isValidPassword) {
            const error = new Error(
              'The email & password combination is incorrect!'
            );
            return done(error);
          }

         return done(null, currentUser);

    }catch(error){
        return done(error);
    }
}));



